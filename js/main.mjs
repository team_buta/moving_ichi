//Document.writeは禁止
//document.write('<img src="image/test/02.png">');
//"はつかわない（コーディング規約どれにするかきめてね
const imagePathList = [
    "image/test/01.png",
    "image/test/02.png",
    "image/test/03.png",
    "image/test/04.png",
    "image/test/05.png",
    "image/test/06.png",
    "image/test/07.png",
    "image/test/08.png",
    "image/test/09.png",
    "image/test/10.png",
    "image/test/11.png",
    "image/test/12.png",
    "image/test/13.png",
    "image/test/14.png",
    "image/test/15.png",
    "image/test/16.png",
    "image/test/17.png",
    "image/test/18.png",
    "image/test/19.png",
    "image/test/20.png",
    "image/test/21.png",
    "image/test/22.png",
    "image/test/23.png",
    "image/test/24.png",
    "image/test/25.png",
    "image/test/26.png",
    "image/test/27.png",
    "image/test/28.png",
    "image/test/29.png",
    "image/test/30.png",
    "image/test/31.png",
    "image/test/32.png",
    "image/test/33.png",
    "image/test/34.png",
    "image/test/35.png",
    "image/test/36.png",
    "image/test/37.png",
    "image/test/38.png",
    "image/test/39.png",
    "image/test/40.png",
    "image/test/41.png",
    "image/test/42.png",
    "image/test/43.png",
    "image/test/44.png",
    "image/test/45.png",
    "image/test/46.png",
    "image/test/47.png",
    "image/test/48.png",
    "image/test/49.png",
    "image/test/50.png",
    "image/test/51.png",
    "image/test/52.png",
    "image/test/53.png",
    "image/test/54.png",
    "image/test/55.png",
    //ここに画像を追加していく
];

//const test = imagePathList[3];

// const imageArray = new Array();
// for (let i = 0; i < imagePathList.length; i++){
//     const image = new Image();
//     image.src = imagePathList[1];
//     imageArray.push(image);
// }

const imageTest= (imagePath) => {
    const image = new Image();
    image.src = imagePath;
    return image;
}

const imageArray = imagePathList.map(imagePath => imageTest(imagePath));

console.log(imageArray[5]);
console.log(document.getElementById("test").src);
document.getElementById("test").src=imagePathList[6];
//document.write("<img src=imageArray[5]>");


